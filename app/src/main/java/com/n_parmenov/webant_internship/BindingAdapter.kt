package com.n_parmenov.webant_internship

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

object BindingAdapter {

    @JvmStatic
    @BindingAdapter("image")
    fun loadImage(view: ImageView, src: String) {
        Glide
            .with(view)
            .load(src)
            .centerCrop()
            .into(view)
    }
}