package com.n_parmenov.webant_internship

object Constants {
    const val pokemonDb = "pokemon_db"
    const val pokemonRemoteKeysDb = "pokemon_remote_keys_db"

    const val flow_tag_success = "SUCCESS"
    const val flow_tag_error = "ERROR"
}