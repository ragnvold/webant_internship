package com.n_parmenov.webant_internship

import com.n_parmenov.webant_internship.domain.entities.SearchPokemonResponse
import io.reactivex.Single

interface NetworkDataSource {
    fun searchPokemonById(pokemonId: String): Single<SearchPokemonResponse>
}