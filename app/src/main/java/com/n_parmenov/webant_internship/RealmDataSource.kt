package com.n_parmenov.webant_internship

import androidx.paging.PagingData
import com.n_parmenov.webant_internship.entities.PokemonEntity
import io.reactivex.Flowable

interface RealmDataSource {
    fun addFavorite(pokemonId: String): Flowable<Boolean>
    fun deleteFavorite(pokemonId: String): Flowable<Boolean>

    fun getFavoriteContentPage(currentPage: Int): Flowable<PagingData<PokemonEntity>>?
}