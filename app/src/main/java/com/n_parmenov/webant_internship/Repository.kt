package com.n_parmenov.webant_internship

import androidx.paging.PagingData
import com.n_parmenov.webant_internship.entities.PokemonEntity
import io.reactivex.Flowable
import io.reactivex.Single

interface Repository {
    fun searchPokemonById(pokemonId: String): Single<PokemonDomain>

    fun fetchHandbookContent(): Flowable<PagingData<PokemonEntity>>
    //fun fetchFavoriteContent(): Flowable<PagingData<PokemonDomain>>

    fun fetchDetailsOfPokemon(pokemonId: String): Single<PokemonDomain>

    /*fun enableCardFavorite(pokemonId: String): Flowable<Boolean>
    fun disableCardFavorite(pokemonId: String): Flowable<Boolean>*/
}