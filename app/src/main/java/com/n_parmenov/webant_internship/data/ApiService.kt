package com.n_parmenov.webant_internship.data

import com.n_parmenov.webant_internship.domain.entities.SearchPokemonResponse
import com.n_parmenov.webant_internship.framework.sources.PageResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("cards")
    fun getCards(@Query("page") currentPage: Int, @Query("pageSize") pageSize: Int = 20): Single<PageResponse>

    @GET("cards/{id}")
    fun searchCardById(@Path("id") id: String): Single<SearchPokemonResponse>

}