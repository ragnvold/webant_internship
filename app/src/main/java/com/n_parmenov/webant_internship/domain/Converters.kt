package com.n_parmenov.webant_internship

import com.n_parmenov.webant_internship.domain.AttackPresentation
import com.n_parmenov.webant_internship.domain.PokemonDetailsPresentation
import com.n_parmenov.webant_internship.domain.PokemonShortsPresentation
import com.n_parmenov.webant_internship.domain.entities.SearchPokemonResponse
import com.n_parmenov.webant_internship.entities.AttackEntity
import com.n_parmenov.webant_internship.entities.PokemonEntity

// POKEMON

// network -> regular
fun Pokemon.toDomain() = PokemonDomain(
    id, false, images.small, name, hp.toInt(), rarity, types[0], subtypes[0], attacks.map { it.toDomain() }
)

fun Pokemon.toEntity() = PokemonEntity(
    id, false, hp.toInt(), images.small, name, rarity, types[0], subtypes[0]
)

fun PokemonDomain.toShortPresentation() = PokemonShortsPresentation(
    id, image, name, hp.toString(), favorite
)

fun PokemonDomain.toDetailsPresentation() = PokemonDetailsPresentation(
    id, image, rare.toString(), type, subtype, hp.toString(), attacks.map { it.toPresentation() }
)

fun PokemonEntity.toDomain() = PokemonDomain(
    pokemonId, favorite, image, name, hp, rare, type, subType, listOf<AttackDomain>()
)

// ATTACK

// network -> regular
fun Attack.toDomain() = AttackDomain(
    name, convertedEnergyCost, damage
)

fun AttackEntity.toDomain() = AttackDomain(
    title, energyCost, damage
)

fun Attack.toEntity() = AttackEntity(
    title = name, energyCost = convertedEnergyCost, damage = damage
)

// domain -> presentation
fun AttackDomain.toPresentation() = AttackPresentation(
    title, energyCost.toString(), damage
)

// SEARCH POKEMON
fun SearchPokemonResponse.toDomain() = PokemonDomain(
    data.id,
    false,
    data.images.small,
    data.name,
    data.hp.toInt(),
    data.rarity,
    data.types[0],
    data.subtypes[0],
    data.attacks.map { it.toDomain() }
)