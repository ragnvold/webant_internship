package com.n_parmenov.webant_internship

import com.google.gson.annotations.SerializedName

data class Pokemon(
    @SerializedName("abilities")
    val abilities: List<Ability>,
    @SerializedName("artist")
    val artist: String,
    @SerializedName("attacks")
    val attacks: List<Attack>,
    @SerializedName("convertedRetreatCost")
    val convertedRetreatCost: Int,
    @SerializedName("evolvesFrom")
    val evolvesFrom: String,
    @SerializedName("flavorText")
    val flavorText: String,
    @SerializedName("hp")
    val hp: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("images")
    val images: Images,
    @SerializedName("legalities")
    val legalities: Legalities,
    @SerializedName("name")
    val name: String,
    @SerializedName("nationalPokedexNumbers")
    val nationalPokedexNumbers: List<Int>,
    @SerializedName("number")
    val number: String,
    @SerializedName("rarity")
    val rarity: String,
    @SerializedName("resistances")
    val resistances: List<Resistance>,
    @SerializedName("retreatCost")
    val retreatCost: List<String>,
    @SerializedName("set")
    val `set`: Set,
    @SerializedName("subtypes")
    val subtypes: List<String>,
    @SerializedName("supertype")
    val supertype: String,
    @SerializedName("tcgplayer")
    val tcgplayer: Tcgplayer,
    @SerializedName("types")
    val types: List<String>,
    @SerializedName("weaknesses")
    val weaknesses: List<Weaknesse>
)

data class Ability(
    @SerializedName("name")
    val name: String,
    @SerializedName("text")
    val text: String,
    @SerializedName("type")
    val type: String
)

data class Attack(
    @SerializedName("convertedEnergyCost")
    val convertedEnergyCost: Int,
    @SerializedName("cost")
    val cost: List<String>,
    @SerializedName("damage")
    val damage: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("text")
    val text: String
)

data class Images(
    @SerializedName("large")
    val large: String,
    @SerializedName("small")
    val small: String
)

data class Legalities(
    @SerializedName("expanded")
    val expanded: String,
    @SerializedName("unlimited")
    val unlimited: String
)

data class Resistance(
    @SerializedName("type")
    val type: String,
    @SerializedName("value")
    val value: String
)

data class Set(
    @SerializedName("id")
    val id: String,
    @SerializedName("images")
    val images: ImagesX,
    @SerializedName("legalities")
    val legalities: LegalitiesX,
    @SerializedName("name")
    val name: String,
    @SerializedName("printedTotal")
    val printedTotal: Int,
    @SerializedName("ptcgoCode")
    val ptcgoCode: String,
    @SerializedName("releaseDate")
    val releaseDate: String,
    @SerializedName("series")
    val series: String,
    @SerializedName("total")
    val total: Int,
    @SerializedName("updatedAt")
    val updatedAt: String
)

data class Tcgplayer(
    @SerializedName("prices")
    val prices: Prices,
    @SerializedName("updatedAt")
    val updatedAt: String,
    @SerializedName("url")
    val url: String
)

data class Weaknesse(
    @SerializedName("type")
    val type: String,
    @SerializedName("value")
    val value: String
)

data class ImagesX(
    @SerializedName("logo")
    val logo: String,
    @SerializedName("symbol")
    val symbol: String
)

data class LegalitiesX(
    @SerializedName("expanded")
    val expanded: String,
    @SerializedName("unlimited")
    val unlimited: String
)

data class Prices(
    @SerializedName("holofoil")
    val holofoil: Holofoil,
    @SerializedName("reverseHolofoil")
    val reverseHolofoil: ReverseHolofoil
)

data class Holofoil(
    @SerializedName("directLow")
    val directLow: Any,
    @SerializedName("high")
    val high: Double,
    @SerializedName("low")
    val low: Double,
    @SerializedName("market")
    val market: Double,
    @SerializedName("mid")
    val mid: Double
)

data class ReverseHolofoil(
    @SerializedName("directLow")
    val directLow: Any,
    @SerializedName("high")
    val high: Double,
    @SerializedName("low")
    val low: Double,
    @SerializedName("market")
    val market: Double,
    @SerializedName("mid")
    val mid: Double
)