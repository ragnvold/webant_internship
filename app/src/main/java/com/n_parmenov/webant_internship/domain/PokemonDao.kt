package com.n_parmenov.webant_internship.domain

import androidx.paging.PagingSource
import androidx.room.*
import com.n_parmenov.webant_internship.entities.*

@Dao
interface PokemonRxDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(pokemons: List<PokemonEntity>)

    @Query("SELECT * FROM pokemons ORDER BY pokemonId ASC")
    fun selectAll(): PagingSource<String, PokemonEntity>

    @Query("DELETE FROM pokemons")
    fun clearPokemons()

    @Query("SELECT * FROM pokemons")
    fun fetchAll(): List<PokemonEntity>
}

@Dao
interface AttackRxDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(attacks: List<AttackEntity>)
}

@Dao
interface PokemonAttackRxDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(relations: List<PokemonAttacksCrossRef>)

    @Transaction
    @Query("SELECT * FROM pokemons")
    fun fetchAttacks(): List<PokemonWithAttacks>
}

@Dao
interface PokemonRemoteKeysRxDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(remoteKey: List<PokemonRemoteKeysEntity>)

    @Query("SELECT * FROM pokemonRemoteKeys WHERE id = :pokemonId")
    fun remoteKeysByPokemonId(pokemonId: String): PokemonRemoteKeysEntity

    @Query("DELETE FROM pokemonRemoteKeys")
    fun clearRemoteKeys()
}