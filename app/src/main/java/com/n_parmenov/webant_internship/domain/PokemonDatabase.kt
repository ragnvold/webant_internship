package com.n_parmenov.webant_internship.domain

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.n_parmenov.webant_internship.entities.AttackEntity
import com.n_parmenov.webant_internship.entities.PokemonAttacksCrossRef
import com.n_parmenov.webant_internship.entities.PokemonEntity
import com.n_parmenov.webant_internship.entities.PokemonRemoteKeysEntity

@Database(
    entities = [PokemonEntity::class, PokemonRemoteKeysEntity::class, AttackEntity::class, PokemonAttacksCrossRef::class],
    version = 1,
    exportSchema = false
)
abstract class PokemonDatabase : RoomDatabase() {
    abstract fun pokemonRxDao(): PokemonRxDao
    abstract fun pokemonRemoteKeysRxDao(): PokemonRemoteKeysRxDao
    abstract fun attackRxDao(): AttackRxDao
    abstract fun pokemonAttackRxDao(): PokemonAttackRxDao

    companion object {
        @Volatile
        private var instance: PokemonDatabase? = null

        fun getInstance(context: Context): PokemonDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also {
                    instance = it
                }
            }
        }

        private fun buildDatabase(context: Context) =
            Room
                .databaseBuilder(
                    context.applicationContext,
                    PokemonDatabase::class.java,
                    "pokemon.db"
                )
                .build()
    }
}