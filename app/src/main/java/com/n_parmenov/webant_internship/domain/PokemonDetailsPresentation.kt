package com.n_parmenov.webant_internship.domain

data class PokemonDetailsPresentation(
    val image: String,
    val name: String,
    val rare: String?,
    val type: String,
    val subtype: String,
    val hp: String,
    val attacks: List<AttackPresentation>
)

data class AttackPresentation(
    val name: String,
    val energyCost: String,
    val damage: String
)