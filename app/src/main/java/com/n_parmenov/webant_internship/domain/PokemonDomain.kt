package com.n_parmenov.webant_internship

data class PokemonDomain(
    val id: String,
    val favorite: Boolean,
    val image: String,
    val name: String,
    val hp: Int,
    val rare: String?,
    val type: String,
    val subtype: String,
    val attacks: List<AttackDomain>
)

data class AttackDomain(
    val title: String,
    val energyCost: Int,
    val damage: String
)