package com.n_parmenov.webant_internship.domain

data class PokemonShortsPresentation(
    val id: String,
    val image: String,
    val name: String,
    val hp: String,
    val favorite: Boolean
)