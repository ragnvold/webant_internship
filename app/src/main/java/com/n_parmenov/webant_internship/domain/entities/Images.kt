package com.n_parmenov.webant_internship.domain.entities


import com.google.gson.annotations.SerializedName

data class Images(
    @SerializedName("large")
    val large: String,
    @SerializedName("small")
    val small: String
)