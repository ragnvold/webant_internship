package com.n_parmenov.webant_internship.domain.entities


import com.google.gson.annotations.SerializedName

data class ImagesX(
    @SerializedName("logo")
    val logo: String,
    @SerializedName("symbol")
    val symbol: String
)