package com.n_parmenov.webant_internship.domain.entities


import com.google.gson.annotations.SerializedName

data class LegalitiesX(
    @SerializedName("expanded")
    val expanded: String,
    @SerializedName("unlimited")
    val unlimited: String
)