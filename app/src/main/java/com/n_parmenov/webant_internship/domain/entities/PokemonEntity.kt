package com.n_parmenov.webant_internship.entities

import androidx.room.*

@Entity(tableName = "pokemons")
data class PokemonEntity(
    @PrimaryKey val pokemonId: String,
    val favorite: Boolean,
    val hp: Int,
    val image: String,
    val name: String,
    val rare: String?,
    val type: String,
    val subType: String
)

@Entity(tableName = "pokemonRemoteKeys")
data class PokemonRemoteKeysEntity(
    @PrimaryKey val id: String,
    val nextKey: Int?,
    val prevKey: Int?
)

@Entity(tableName = "attacks")
data class AttackEntity(
    @PrimaryKey(autoGenerate = true) val attackId: Int = 0,
    val title: String,
    val energyCost: Int,
    val damage: String
)

data class PokemonWithAttacks(
    @Embedded val pokemon: PokemonEntity,
    @Relation(
        parentColumn = "pokemonId",
        entityColumn = "attackId",
        associateBy = Junction(PokemonAttacksCrossRef::class)
    ) val attacks: List<AttackEntity>
)

@Entity(tableName = "pokemonAttacks", primaryKeys = ["pokemonId", "attackId"])
data class PokemonAttacksCrossRef(
    val pokemonId: String,
    val attackId: Int
)