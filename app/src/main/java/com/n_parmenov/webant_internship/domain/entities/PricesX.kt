package com.n_parmenov.webant_internship.domain.entities


import com.google.gson.annotations.SerializedName

data class PricesX(
    @SerializedName("holofoil")
    val holofoil: Holofoil,
    @SerializedName("reverseHolofoil")
    val reverseHolofoil: ReverseHolofoil
)