package com.n_parmenov.webant_internship.domain.entities


import com.google.gson.annotations.SerializedName

data class SearchPokemonResponse(
    @SerializedName("data")
    val data: Data
)