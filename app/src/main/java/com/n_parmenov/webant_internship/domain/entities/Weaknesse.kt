package com.n_parmenov.webant_internship.domain.entities


import com.google.gson.annotations.SerializedName

data class Weaknesse(
    @SerializedName("type")
    val type: String,
    @SerializedName("value")
    val value: String
)