package com.n_parmenov.webant_internship.framework.mediator

import android.util.Log
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.rxjava2.RxRemoteMediator
import com.n_parmenov.webant_internship.Pokemon
import com.n_parmenov.webant_internship.data.Network
import com.n_parmenov.webant_internship.domain.PokemonDatabase
import com.n_parmenov.webant_internship.entities.PokemonEntity
import com.n_parmenov.webant_internship.entities.PokemonRemoteKeysEntity
import com.n_parmenov.webant_internship.toEntity
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

@ExperimentalPagingApi
class PokemonContentRemoteMediator(private val database: PokemonDatabase) :
    RxRemoteMediator<String, PokemonEntity>() {

    companion object {
        const val INVALID_PAGE = -1
        const val DEFAULT_PAGE = 1
    }

    private val service by lazy { Network.provideRetrofit() }

    override fun loadSingle(
        loadType: LoadType,
        state: PagingState<String, PokemonEntity>
    ): Single<MediatorResult> {
        return Single.just(loadType)
            .subscribeOn(Schedulers.io())
            .map {
                when (loadType) {
                    LoadType.REFRESH -> {
                        val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                        Log.i("DATA_FLOW", "refresh " + remoteKeys?.nextKey)
                        remoteKeys?.nextKey?.minus(1) ?: DEFAULT_PAGE
                    }
                    LoadType.PREPEND -> {
                        val remoteKeys = getRemoteKeyForFirstItem(state)
                        Log.i("DATA_FLOW", "prepend " + remoteKeys?.prevKey)
                        remoteKeys?.prevKey ?: INVALID_PAGE
                    }
                    LoadType.APPEND -> {
                        val remoteKeys = getRemoteKeyForLastItem(state)
                        Log.i("DATA_FLOW", "append " + remoteKeys?.nextKey)
                        remoteKeys?.nextKey ?: INVALID_PAGE
                    }
                }
            }
            .flatMap { page ->
                if (page == INVALID_PAGE) {
                    Single.just(MediatorResult.Success(endOfPaginationReached = true))
                } else {
                    val response = service.getCards(page, state.config.pageSize)
                    val isEndPage =
                        response.map { isEndPage(it.page, it.pageSize, it.count, it.totalCount) }

                    response
                        .map {
                            insertPage(
                                isEndPage = isEndPage.blockingGet(),
                                response.blockingGet().page,
                                loadType,
                                it.data
                            )
                        }
                        .map<MediatorResult> { MediatorResult.Success(endOfPaginationReached = isEndPage.blockingGet()) }
                        .onErrorReturn { MediatorResult.Error(it) }
                }
            }
            .onErrorReturn { MediatorResult.Error(it) }
    }

    private fun isEndPage(
        currentPage: Int,
        pageSize: Int,
        currentPageItemsCount: Int,
        totalItemsCount: Int
    ): Boolean {
        return (currentPage * pageSize + currentPageItemsCount) == totalItemsCount
    }

    private fun insertPage(
        isEndPage: Boolean,
        page: Int,
        loadType: LoadType,
        data: List<Pokemon>
    ): List<Pokemon> {
        try {
            database.runInTransaction {
                if (loadType == LoadType.REFRESH) {
                    database.pokemonRxDao().clearPokemons()
                    database.pokemonRemoteKeysRxDao().clearRemoteKeys()
                }

                Log.i(
                    "DATA_FLOW",
                    "before insert => " + database.pokemonRxDao().fetchAll().size.toString()
                )

                val prevKey = if (page == 1) null else page - 1
                val nextKey = if (isEndPage) null else page + 1
                val keys = data.map { PokemonRemoteKeysEntity(it.id, nextKey, prevKey) }
                val pokemons = data.map { it.toEntity() }

                /*keys.map {
                    Log.i("DATA_FLOW", "${it.id} => ${it.prevKey} => ${it.nextKey}")
                }*/

                database.pokemonRemoteKeysRxDao().insertAll(keys)
                database.pokemonRxDao().insertAll(pokemons)

                Log.i(
                    "DATA_FLOW",
                    "after insert => " + database.pokemonRxDao().fetchAll().size.toString()
                )
            }
        } catch (exception: Exception) {
            Log.i("DATA_FLOW", "exc => ${exception.localizedMessage}")
            throw Exception(exception.message)
        }

        return data
    }

    private fun getRemoteKeyForFirstItem(state: PagingState<String, PokemonEntity>): PokemonRemoteKeysEntity? {
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
            ?.let { pokemon ->
                database.pokemonRemoteKeysRxDao().remoteKeysByPokemonId(pokemon.pokemonId)
            }
    }

    private fun getRemoteKeyForLastItem(state: PagingState<String, PokemonEntity>): PokemonRemoteKeysEntity? {
        val lastPokemon = state.pages.lastOrNull { it.data.isNotEmpty() }?.data?.lastOrNull()
        Log.i("DATA_FLOW", "last item => $lastPokemon")
        return state.pages.lastOrNull { it.data.isNotEmpty() }?.data?.lastOrNull()?.let { pokemon ->
            val result = database.pokemonRemoteKeysRxDao().remoteKeysByPokemonId(pokemon.pokemonId)
            Log.i("DATA_FLOW", result.toString())
            result
        }
    }

    private fun getRemoteKeyClosestToCurrentPosition(state: PagingState<String, PokemonEntity>): PokemonRemoteKeysEntity? {
        return state.anchorPosition?.let { pos ->
            state.closestItemToPosition(pos)?.pokemonId?.let { id ->
                database.pokemonRemoteKeysRxDao().remoteKeysByPokemonId(id)
            }
        }
    }
}