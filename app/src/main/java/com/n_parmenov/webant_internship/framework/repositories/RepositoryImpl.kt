package com.n_parmenov.webant_internship.framework.repositories

import android.util.Log
import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.rxjava2.flowable
import com.n_parmenov.webant_internship.PokemonDomain
import com.n_parmenov.webant_internship.Repository
import com.n_parmenov.webant_internship.domain.PokemonDatabase
import com.n_parmenov.webant_internship.entities.PokemonEntity
import com.n_parmenov.webant_internship.framework.mediator.PokemonContentRemoteMediator
import com.n_parmenov.webant_internship.framework.sources.NetworkDataSourceImpl
import com.n_parmenov.webant_internship.toDomain
import io.reactivex.Flowable
import io.reactivex.Single
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@ExperimentalPagingApi
class RepositoryImpl(private val database: PokemonDatabase) : Repository {

    private val networkDataSource by lazy { NetworkDataSourceImpl() }
    private val pokemonContentRemoteMediator by lazy { PokemonContentRemoteMediator(database) }

    override fun fetchHandbookContent(): Flowable<PagingData<PokemonEntity>> {
        Log.i("DATA_FLOW", "repository")
        return Pager(
            config = PagingConfig(
                pageSize = 20,
                enablePlaceholders = true,
                maxSize = 5 + 2 * 20,
                prefetchDistance = 5,
                initialLoadSize = 20
            ),
            remoteMediator = pokemonContentRemoteMediator,
            pagingSourceFactory = { database.pokemonRxDao().selectAll() }
        ).flowable
    }

    override fun fetchDetailsOfPokemon(pokemonId: String): Single<PokemonDomain> =
        networkDataSource.searchPokemonById(pokemonId).map { it.toDomain() }

    override fun searchPokemonById(pokemonId: String): Single<PokemonDomain> =
        networkDataSource.searchPokemonById(pokemonId).map { it.toDomain() }

    /*override fun enableCardFavorite(pokemonId: String): Flowable<Boolean> =
        realmDataSource.addFavorite(pokemonId)

    override fun disableCardFavorite(pokemonId: String): Flowable<Boolean> =
        realmDataSource.deleteFavorite(pokemonId)*/
}