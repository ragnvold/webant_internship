package com.n_parmenov.webant_internship.framework.sources

import com.n_parmenov.webant_internship.NetworkDataSource
import com.n_parmenov.webant_internship.Pokemon
import com.n_parmenov.webant_internship.data.Network
import com.n_parmenov.webant_internship.domain.entities.SearchPokemonResponse
import io.reactivex.Single

class NetworkDataSourceImpl: NetworkDataSource {

    private val service by lazy { Network.provideRetrofit() }

    override fun searchPokemonById(pokemonId: String): Single<SearchPokemonResponse> = service.searchCardById(pokemonId)
}