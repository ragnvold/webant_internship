package com.n_parmenov.webant_internship.framework.sources

import androidx.paging.PagingState
import androidx.paging.rxjava2.RxPagingSource
import com.n_parmenov.webant_internship.Pokemon
import com.n_parmenov.webant_internship.PokemonDomain
import com.n_parmenov.webant_internship.data.Network
import com.n_parmenov.webant_internship.toDomain
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

data class PageResponse(
    val data: List<Pokemon>,
    val page: Int,
    val pageSize: Int,
    val count: Int,
    val totalCount: Int
)

class NetworkPagingDataSourceImpl : RxPagingSource<Int, PokemonDomain>() {

    private val service by lazy { Network.provideRetrofit() }

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, PokemonDomain>> {
        val position = params.key ?: 1

        return service.getCards(position)
            .subscribeOn(Schedulers.io())
            .map { toLoadResult(it, position) }
            .onErrorReturn { LoadResult.Error(it) }
    }

    private fun toLoadResult(responsePage: PageResponse, position: Int): LoadResult<Int, PokemonDomain> {
        return LoadResult.Page(
            data = responsePage.data.map { it.toDomain() },
            prevKey = if (position == 1) null else position - 1,
            nextKey = if (position == responsePage.totalCount) null else position + 1
        )
    }

    override fun getRefreshKey(state: PagingState<Int, PokemonDomain>): Int? =
        state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
}