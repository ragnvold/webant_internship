package com.n_parmenov.webant_internship.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.n_parmenov.webant_internship.domain.AttackPresentation
import com.n_parmenov.webant_internship.databinding.CardPokemonAttackBinding

class DetailsAttackAdapter: RecyclerView.Adapter<DetailsAttackAdapter.AttackViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AttackViewHolder = AttackViewHolder(
        CardPokemonAttackBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: AttackViewHolder, position: Int) =
        holder.bind(attacks[position])

    override fun getItemCount(): Int = attacks.size

    inner class AttackViewHolder(private val itemBinding: CardPokemonAttackBinding): RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(data: AttackPresentation) {
            itemBinding.data = data
        }
    }

    companion object {
        private var attacks = emptyList<AttackPresentation>()

        fun setData(newList: List<AttackPresentation>) {
            attacks = newList
        }
    }
}