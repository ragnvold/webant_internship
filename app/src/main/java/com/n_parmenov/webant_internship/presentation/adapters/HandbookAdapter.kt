package com.n_parmenov.webant_internship.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.n_parmenov.webant_internship.databinding.CardPokemonBinding
import com.n_parmenov.webant_internship.domain.PokemonShortsPresentation

class HandbookAdapter(private val favoriteCallback: (String, Boolean) -> Unit) :
    PagingDataAdapter<PokemonShortsPresentation, HandbookAdapter.PokemonShortsViewHolder>(
        PokemonShortsComparator
    ) {

    override fun onBindViewHolder(holder: PokemonShortsViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonShortsViewHolder {
        return PokemonShortsViewHolder(
            CardPokemonBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    inner class PokemonShortsViewHolder(private val itemBinding: CardPokemonBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(item: PokemonShortsPresentation) = with(itemBinding) {
            data = item
            /*favorite.setOnClickListener {
                val temp = data.copy(favorite = !data.favorite)
                favoriteCallback(data.id, temp.favorite)
                data = temp
            }*/
        }
    }

    object PokemonShortsComparator : DiffUtil.ItemCallback<PokemonShortsPresentation>() {
        override fun areItemsTheSame(
            oldItem: PokemonShortsPresentation,
            newItem: PokemonShortsPresentation
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: PokemonShortsPresentation,
            newItem: PokemonShortsPresentation
        ): Boolean {
            return oldItem == newItem
        }
    }
}