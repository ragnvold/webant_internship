package com.n_parmenov.webant_internship.presentation.adapters

import androidx.paging.PagingData

interface RecyclerAdapter<T : Any> {
    fun updateData(newData: PagingData<T>)
}
