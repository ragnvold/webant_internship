package com.n_parmenov.webant_internship.presentation.presenters

//import com.n_parmenov.webant_internship.usecases.GetFavoriteContentUseCase
import androidx.paging.ExperimentalPagingApi
import com.n_parmenov.webant_internship.Repository
import com.n_parmenov.webant_internship.domain.PokemonDatabase
import com.n_parmenov.webant_internship.framework.repositories.RepositoryImpl
import com.n_parmenov.webant_internship.presentation.views.contracts.Favorite
import io.reactivex.disposables.CompositeDisposable

class FavoritePresenter(private val view: Favorite.View, private val database: PokemonDatabase): Favorite.Presenter {

    @ExperimentalPagingApi
    private val repository: Repository by lazy { RepositoryImpl(database) }
    //private val favoriteContentUseCase by lazy { GetFavoriteContentUseCase(repository) }
    private val dispose by lazy { CompositeDisposable() }

    override fun loadContent() {
        /*dispose.add(favoriteContentUseCase.invoke().subscribe { data ->
            //view.showData(data.map { it.toShortPresentation() })
        })*/
    }
}