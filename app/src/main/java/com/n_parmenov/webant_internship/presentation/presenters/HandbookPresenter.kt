package com.n_parmenov.webant_internship.presentation.presenters

//import com.n_parmenov.webant_internship.usecases.MakeFavoriteContentUseCase
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingData
import androidx.paging.map
import androidx.paging.rxjava2.cachedIn
import com.n_parmenov.webant_internship.Constants.flow_tag_error
import com.n_parmenov.webant_internship.Constants.flow_tag_success
import com.n_parmenov.webant_internship.domain.PokemonDatabase
import com.n_parmenov.webant_internship.domain.PokemonShortsPresentation
import com.n_parmenov.webant_internship.framework.repositories.RepositoryImpl
import com.n_parmenov.webant_internship.presentation.views.contracts.Handbook
import com.n_parmenov.webant_internship.toShortPresentation
import com.n_parmenov.webant_internship.usecases.GetHandbookContentUseCase
import com.n_parmenov.webant_internship.usecases.SearchUseCase
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalPagingApi
@ExperimentalCoroutinesApi
class HandbookPresenter(private val view: Handbook.View, private val database: PokemonDatabase) : Handbook.Presenter {

    private val repository by lazy { RepositoryImpl(database) }
    private val getHandbookContentUseCase by lazy { GetHandbookContentUseCase(repository) }
    //private val makePokemonItemFavoriteUseCase by lazy { MakeFavoriteContentUseCase(repository) }
    private val searchUseCase by lazy { SearchUseCase(repository) }
    private val disposable by lazy { CompositeDisposable() }

    override fun getHandbookContent(): Flowable<PagingData<PokemonShortsPresentation>> {
        return getHandbookContentUseCase().map { data -> data.map { it.toShortPresentation() } }
    }

    override fun makeFavorite(id: String, makeFavorite: Boolean) {
        //disposable.add(makePokemonItemFavoriteUseCase(id, makeFavorite).subscribe())
    }

    fun searchPokemon() {
        disposable.add(
            searchUseCase("xy7-54")
                .subscribeOn(Schedulers.io()).subscribe({ success ->
                    Log.i(flow_tag_success, success.toString())
                }, { error ->
                    Log.i(flow_tag_error, error.message.toString())
                })
        )
    }
}

@ExperimentalPagingApi
@ExperimentalCoroutinesApi
class HandbookViewModel(private val view: Handbook.View, private val database: PokemonDatabase): ViewModel(), Handbook.Presenter {

    private val repository by lazy { RepositoryImpl(database) }
    private val getHandbookContentUseCase by lazy { GetHandbookContentUseCase(repository) }

    override fun getHandbookContent(): Flowable<PagingData<PokemonShortsPresentation>> {
        return getHandbookContentUseCase().map { it.map { item -> item.toShortPresentation() } }.cachedIn(viewModelScope)
    }

    override fun makeFavorite(id: String, makeFavorite: Boolean) {
        TODO("Not yet implemented")
    }
}