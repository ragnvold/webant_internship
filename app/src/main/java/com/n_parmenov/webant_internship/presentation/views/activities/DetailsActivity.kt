package com.n_parmenov.webant_internship.presentation.views.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.n_parmenov.webant_internship.databinding.ActivityDetailsBinding
import com.n_parmenov.webant_internship.presentation.presenters.DetailsPresentation
import com.n_parmenov.webant_internship.presentation.views.contracts.Details

class DetailsActivity: AppCompatActivity(), Details.View {

    private val presenter: Details.Presenter by lazy { DetailsPresentation() }
    private lateinit var binding: ActivityDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsBinding.inflate(layoutInflater)
    }

    private fun backToMain() {
        finish()
    }
}