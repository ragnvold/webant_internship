package com.n_parmenov.webant_internship.presentation.views.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.paging.ExperimentalPagingApi
import com.n_parmenov.webant_internship.R
import com.n_parmenov.webant_internship.databinding.ActivityMainBinding
import com.n_parmenov.webant_internship.presentation.views.contracts.Main
import com.n_parmenov.webant_internship.presentation.views.fragments.FavoriteFragment
import com.n_parmenov.webant_internship.presentation.views.fragments.HandbookFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@ExperimentalPagingApi
class MainActivity : AppCompatActivity(), Main.View {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        fragmentSwitch(HandbookFragment())
        binding.bottomBar.setOnItemSelectedListener { item ->
            val newFragment = when (item.itemId) {
                R.id.navigation_handbook -> HandbookFragment.getInstance()
                R.id.navigation_favorite -> FavoriteFragment()
                else -> HandbookFragment.getInstance()
            }
            fragmentSwitch(newFragment)

            true
        }
    }

    private fun fragmentSwitch(fragmentInstance: Fragment) {
        val manager = supportFragmentManager
        manager.beginTransaction().replace(R.id.fragment_container, fragmentInstance).commit()
    }

    private fun toDetails() {
        startActivity(Intent(this, DetailsActivity::class.java))
    }
}