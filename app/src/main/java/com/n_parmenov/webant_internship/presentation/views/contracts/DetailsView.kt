package com.n_parmenov.webant_internship.presentation.views.contracts

interface Details {
    interface View {}
    interface Presenter {}
}