package com.n_parmenov.webant_internship.presentation.views.contracts

import androidx.paging.PagingData
import com.n_parmenov.webant_internship.domain.PokemonShortsPresentation

interface Favorite {
    interface View {
        fun showData(data: PagingData<PokemonShortsPresentation>)
        fun showError()
    }
    interface Presenter {
        fun loadContent()
    }
}