package com.n_parmenov.webant_internship.presentation.views.contracts

import androidx.paging.PagingData
import com.n_parmenov.webant_internship.domain.PokemonShortsPresentation
import io.reactivex.Flowable

interface Handbook {
    interface View

    interface Presenter {
        fun getHandbookContent(): Flowable<PagingData<PokemonShortsPresentation>>
        fun makeFavorite(id: String, makeFavorite: Boolean)
    }
}