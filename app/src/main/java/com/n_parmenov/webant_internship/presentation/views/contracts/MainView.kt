package com.n_parmenov.webant_internship.presentation.views.contracts

interface Main {
    interface View {}
    interface Presenter {}
}