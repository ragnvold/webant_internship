package com.n_parmenov.webant_internship.presentation.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.paging.PagingData
import com.n_parmenov.webant_internship.R
import com.n_parmenov.webant_internship.databinding.FragmentFavoriteBinding
import com.n_parmenov.webant_internship.domain.PokemonShortsPresentation
import com.n_parmenov.webant_internship.presentation.adapters.FavoriteAdapter
import com.n_parmenov.webant_internship.presentation.views.contracts.Favorite

class FavoriteFragment: Fragment(), Favorite.View {

    private lateinit var binding: FragmentFavoriteBinding
    private lateinit var adapter: FavoriteAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorite, container, false)
        adapter = FavoriteAdapter()

        return binding.root
    }

    override fun showData(data: PagingData<PokemonShortsPresentation>) {
        TODO("Not yet implemented")
    }

    override fun showError() {
        TODO("Not yet implemented")
    }
}