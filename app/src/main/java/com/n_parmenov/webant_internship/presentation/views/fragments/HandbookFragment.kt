package com.n_parmenov.webant_internship.presentation.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.paging.ExperimentalPagingApi
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.n_parmenov.webant_internship.R
import com.n_parmenov.webant_internship.databinding.FragmentHandbookBinding
import com.n_parmenov.webant_internship.domain.PokemonDatabase
import com.n_parmenov.webant_internship.presentation.adapters.HandbookAdapter
import com.n_parmenov.webant_internship.presentation.presenters.HandbookViewModel
import com.n_parmenov.webant_internship.presentation.views.contracts.Handbook
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@ExperimentalPagingApi
class HandbookFragment : Fragment(), Handbook.View {

    private val database by lazy { PokemonDatabase.getInstance(requireContext()) }

    private val presenter by lazy { HandbookViewModel(this, database) }
    private lateinit var binding: FragmentHandbookBinding
    private lateinit var adapter: HandbookAdapter

    private val disposable by lazy { CompositeDisposable() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_handbook, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = HandbookAdapter { id, makeFavorite ->
            //presenter.makeFavorite(id, makeFavorite)
        }
        val llm = GridLayoutManager(this.context, 2, LinearLayoutManager.VERTICAL, false)

        binding.handbookList.apply {
            layoutManager = llm
            adapter = this@HandbookFragment.adapter
        }

        disposable.add(presenter.getHandbookContent().subscribe {
            adapter.submitData(lifecycle, it)
        })

    }

    override fun onDestroyView() {
        disposable.dispose()
        super.onDestroyView()
    }

    companion object {
        private var instance: HandbookFragment? = null

        fun getInstance(): HandbookFragment {
            if (instance == null) {
                instance = HandbookFragment()
            }

            return instance!!
        }
    }
}