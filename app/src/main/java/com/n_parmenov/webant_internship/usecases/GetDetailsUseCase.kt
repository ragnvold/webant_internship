package com.n_parmenov.webant_internship.usecases

import com.n_parmenov.webant_internship.Repository

class GetDetailsUseCase(private val repository: Repository) {
    operator fun invoke(pokemonId: String) = repository.fetchDetailsOfPokemon(pokemonId)
}