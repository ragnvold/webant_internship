package com.n_parmenov.webant_internship.usecases

import androidx.paging.PagingData
import androidx.paging.map
import com.n_parmenov.webant_internship.PokemonDomain
import com.n_parmenov.webant_internship.Repository
import com.n_parmenov.webant_internship.toDomain
import io.reactivex.Flowable

class GetHandbookContentUseCase(private val repository: Repository) {
    operator fun invoke(): Flowable<PagingData<PokemonDomain>> = repository.fetchHandbookContent().map { pagingData -> pagingData.map { it.toDomain() } }
}