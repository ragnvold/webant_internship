package com.n_parmenov.webant_internship.usecases

import com.n_parmenov.webant_internship.Repository

class SearchUseCase(private val repository: Repository) {
    operator fun invoke(queryId: String) = repository.searchPokemonById(queryId)
}